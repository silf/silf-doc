Mercury
=======

Description
-----------

This component contains a javascript client for all our experimenrs.


Repository
----------

Project repository is located at: https://bitbucket.org/silf/frontend-mercury.

License
-------

This module is licensed under :download:`Apache 2.0 </licenses/apache-2.0.txt>`
License.

