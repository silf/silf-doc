Silf Backend Commons
====================

Description
-----------

This component is common library for all experiments, it contains:

* Classes for generating ``XMPP`` and ``json`` data sent to the client.
* ``API`` classes for generating experiments (these are optional to use,
  as anything that speaks our protocol can be a server).
* Protocol documentation.
* Some common drivers.

Documentation highlights
------------------------

* :ref:`Module Documentation root <commons:main-page>`
* Protocol description:

  * Currently used protocol version (0.5)

    * :ref:`Conversation between user and experiment <commons:protocol-conversation-0.5>`
    * :ref:`Protocol details <commons:silf-protocol-0.5>`

* :ref:`Experiment creation tutorial <commons:experiment-tutorial>`

Repository
----------

Project repository is located at: https://bitbucket.org/silf/silf-backend-commons.

License
-------

This module is licensed under :download:`Apache 2.0 </licenses/apache-2.0.txt>`
License.




