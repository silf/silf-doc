Silf app
========

Description
-----------

Django application containgig:

* Database schema,
* Web API
* Admin site
* Means to deploy Mercury

Documentation is  :ref:`located here<athena:main-page>`

Repository
----------

Project repository is located at: 'https://bitbucket.org/silf/silf-backend-athena'.

License
-------

This module is licensed under :download:`Apache 2.0 </licenses/apache-2.0.txt>`
License.
