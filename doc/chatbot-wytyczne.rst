Wytyczne do stworzena chatbota
==============================

Słownik
-------

pokój do chatu
    Pokój XMPP na którym każdy ma uprawnienia do pisania wiadomości,
    służy on do komunikacji pomiędzy użytkownikami w języku naturalnym.
pokój kontrolny
    Pokój XMPP na którym uprawnienia do pisania ma: bot kontrolny Athena,
    opisywany chatbot oraz osoba posiadająca w danej chwili rezerwację.

Wymagania funkcjonalne
----------------------

Ogólnie
*******

W pierwszej wersji bot ma reagować na komendy oraz służyć do przeprowadzenia
ćwiczenia.

Bot działa w dwóch trybach w pierwszym to użytkownik posiadający rezerwacje na
pokoju prosi go o rozpoczęcie pomiarów, w drugim o określonych godzinach
bot sam robi pomiary i je opisuje.

Procedura przeprowadzania doświadczenia (ogólnie)
*************************************************

Bot działa na dwóch pokojach MUC XMPP, pierwszym jest pokój na którym może
rozmawiać z użytkownikiem, drugim jest pokój kontrolny doświadczenia.

Na pokoju kontrolnym doświadczenia rozmowa jest w dialekcie XMPP opisanym w
:ref:`dokumentacji <commons:protocol-conversation-1.0>`.

Bot ma zdefiniowaną kilka serii pomiarów --- które mogą jakoś od siebie zależeć, dla
poszczególnych eksperymentów mogą być to:

* Eksperyment geigera:

 * Wykonanie doświadczenia z dużym krokiem pomiarowym (30V) od 300 do 900
 * Bo zlokalizowaniu punktów przegięć z poptrzedniej serii zbadanie ich z
   krokiem 2V (seria od 300 do 400V) i od (800V do 900V).

* Osłabienie promieniowania gamma:

 * Wykonanie pomiarów tła
 * Seria pomiarowa dla poszczególnych materiałów

* Ciało doskonale czarne

 * Zebranie widma żarówki dla różnych temperatur

W poszczególnych wypadkach możliwe jest że dopiero po wykonaiu poprzedniej serii
pomiarowej umiemy wykonać kolejne, albo też wiemy ile jest serii pomiarowych
(najpierw zbieramy charakterystykę licznika całego a potem wiadomo w na których
miejscach są przegięcia i można się skoncentrować).

Serię zaczynamy kiedy **osoba uprawniona** prosi nas o rozpoczęcie serii
pomiarowej, osobą taką jest każdy kto ma uprawnienia do pisania na pokoju
kontorlnym

Procedura przeprowadzania doświadczenia (geiger)
************************************************

Użytkownik A ma rezerwację 

* **User A:** *wchodzi do pokoju*
* **Bot:** Cześć User A! Nazywam się Czesio i jestem botem SILF. By ze mną porozmawiać
  wpisz ``\czesio``
* **User A:** ``\czesio``
* **Bot:** Możesz ze mną przeprowadzić doświadczenie "Charakterystyka Licznika
  Geigera Mullera". Jeśli chcesz poznać ogólne informacje o doświadczeniu
  wpisz ``\czesio info``, jeśli chcesz zobaczyć co mogę Ci pokazać w tym doświadczeniu
  wpisz ``\czesio serie``.
* **User A:** ``\czesio`` info
* **Bot:**: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  Nullam tristique nulla sit amet ante facilisis cursus.
  Vestibulum congue convallis leo, ac consequat diam auctor nec. Pełny opis
  przeprowadzania doświadczenia znajduje się pod adresem: http://dolor.sit.amet.pw.edu.pl.
* **User B:** *wchodzi do pokoju*
* **Bot:** Cześć User B! Nazywam się Czesio i jestem botem SILF. By ze mną porozmawiać
  wpisz ``\czesio``
* **User B:** ``\czesio``
* **Bot:** Po uzyskaniu rezerwacji będziesz mógł przeprowadzić ze mną doświadczenie
  "Charakterystyka Licznika Geigera Mullera". Jeśli chcesz poznać ogólne
  informacje o doświadczeniu wpisz ``\czesio info``.
* **User B:** ``\czesio info``
* **Bot:**: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  Nullam tristique nulla sit amet ante facilisis cursus.
  Vestibulum congue convallis leo, ac consequat diam auctor nec. Pełny opis
  przeprowadzania doświadczenia znajduje się pod adresem:
  http://dolor.sit.amet.pw.edu.pl.
* **User A:** ``\czesio serie``
* **Bot:**: Dobrze że pytasz! Mogę teraz wykonać tylko jedną serię pomiarową:
* **Bot:**: Seria 1: Skan ogólny całego przedziału napięć. By ją wykonać napisz
  ``\czesio seria ogolna``.
* **User B:**: ``\czesio seria ogolna``
* **Bot:** Przykro mi takie uprawnienia ma tylko osoba będąca operatorem pokoju.
* **User A:** ``\czesio seria ogolna``
* **Bot:** OK. Startujemy serię pomiarową! Seria ta dokona ogólnego skanu i
  pokaże jak zależy ilość zliczeń od napięcia na lampie w całym zakresie napięć.
  Od tej chwili nie masz możliwości kondtolowania doświadczenia, ponieważ 
  kontroluję go ja. By odzyskać kontrolę napisz ``\czesio seria stop``.
* **User B**:  ``\czesio seria stop``
* **Bot:** Przykro mi takie uprawnienia ma tylko osoba będąca operatorem pokoju.

(...)

* **Bot:** *(w reakcji na wyniki doświadczenia)* Na licznik GM nie rejestruje
  żadnych zliczeń. Jest to normalne --- napięcie na liczniku jest za niskie.

(...)

* **Bot:** *(w reakcji na wyniki doświadczenia)* Jak widzisz przy do 400 V nie
  było zliczeń, teraz na 430 pojawiają się zliczenia.

(...)

* **Bot:** Na tym obszarze znajduje się tak zwane plateaou: ilość zliczeń co
  prawda rośnie ze wzrostem napięca ale bardzo powoli.

(...)

* **Bot:** Tutaj ilość zliczeń rośnie bardzo gwałtownie, jest to wynik maecenas
  gravida tempus varius. Phasellus aliquet dignissim sapien sit amet fermentum.
  Sed sollicitudin pharetra metus, ut euismod enim consequat at.

(...)

* **Bot:** Seria pomiarowa: "Seria ogólna" skończyla się. Przywracam Ci
  uprawnienia operatora pokoju. Po pierwszym skanie odblokowały się takie serie
  pomiarowe:
* **Bot:**: Seria 1: Skan ogólny całego przedziału napięć. By ją wykonać napisz
  ``\czesio seria ogolna``.
* **Bot:**: Seria 2: Skan przejścia między zerową liczbą zliczeń a plateaou.
  By ją wykonać napisz ``\czesio seria plateaou``.
* **Bot:**: Seria 3: Skan przejścia między plateaou a przesyceniem licznika.
  By ją wykonać napisz ``\czesio seria cascade``.

(...)

Różne inne funkcjonalności bota
*******************************

**Kto ma uprawnienia do wydawania komend botowi**
    Na pokoju każdy będzie miał możliwość napisania czegokolwiek do bota bot
    musi wiedzieć kto ma uprawnienia do jakiej komendy.
**Jakie są poziomy uprawnień?**
    Są dwa poziomy uprawnień, albo ktoś ma uprawnienia do zmiany nastaw
    eksperymentu (wtedy może startować serie pomiarowe) albo nie ma tych
    uprawnień. Tylko osoby z uprawnieniami mogą wykonywać polecenia zmieniające
    stan eksperymentu ``\czesio seria *``.
**Skąd bot wie kto ma jakie uprawnienie**
    Wszystkie osoby mają niższy poziom uprawnień, poza: osobą posiadającą
    rezerwację oraz botem :ref:`Atheną <athena:main-page>`.
    Osoba która posiada wyższy poziom uprawnień to osoba która:

    * Posiada głos w pokoju kontrolnym
    * **LUB** Posiadała głos w pokoju kontrolnym przed tym jak bot odebrał
      jej głos do przeprowadzenia sesji.

**Oddawanie użytkownikowi głosu na pokoju po zakończeniu serii**

   Bot oddaje głos użytkownikowi po zakończeniu przeprowadzania danej serii
   pomiarowej. Jednak jeśli w między czasie otrzymał komendę ``silf:experiment:stop``
   to owego głosu nie oddaje.

**W jakich warunkach kończymy serię pomiarową rozpoczętą przez bota**
    Serię kończymy w przypadku:

    * Użytkownik kończy serię pisząc ``\czesio series stop``.
    * Ktokolwiek kończy serię wysyłając: :ref:`commons:proto-v1.0-silf-series-stop`.
      Generalnie może być to eksperyment (ktory kończy serię pomiarową).
    * Ktokolwiek kończy cały eksperyment wysyłając: :ref:`commons:proto-v1.0-silf-experiment-stop`

**Skoro niektóre serie wymagają pierwszego przebiegu kiedy powinno się je zapomnieć**
    Bot powinien resetować swoją pamięć po otrzymaniu ``silf:experiment:stop``.
**Co bot powinien zrobić jeśli dostanie nieoczekiwane wyniki**

    Powinien napisać coś w stylu:

    * **Bot:** O motyla noga! Po 400V powinny pojawić się zliczenia na liczniku GM,
      jeśli do 500V nie pojawią się kolejne zliczenia to prawdopodobnie oznacza
      to awarię licznika.

    (...)

    * **Bot:** O motyla noga! Ciągle nie ma zliczeń. Poczekaj wysyłam informacje
      do administratorów.

    (...)

    * **Bot:** Administratorzy już powiadomieni. Wyłączam teraz eksperyment,
      mozesz z nim sam poprawcować lub uruchomić serię od nowa: możliwe
      że błąd był tylko tymczasowy i sam zaraz przejdzie. Możesz również 
      kliknąć przycisk zakończ i po chwili połączyć się ponownie, to też 
      czasem pomaga.

Sprawy techniczne
-----------------

Wymagania techniczne:

* Kod powinien być napisany w ``Pythonie 3.3``
* Kod powinien być napisany zgodnie z formatowaniem
  `PEP8 <http://legacy.python.org/dev/peps/pep-0008/>`_.
* Najlepiej byłoby napisać główną część bota jako serię pluginow do Atheny, ale możliwe
  że łatwiej będize wykorzystać jakiegoś innego bota napisanego w Pythonie,
  na przykład takiego: `err <https://github.com/gbin/err>`_ (nie testowałem).
* Cały kod musi być tworzony w serwisie `bitbucket <https://bitbucket.org/silf/>`_,
  załóż konto nadamy uprawnienia.
* Cały kod powinien przejść proces Code Review.


Inne przydatne informacje:

* Raczej struktura powinna być taka że jest 1 główny bot, który definiuje API
  pluginów oraz  13 pluginów na eksperymenty

Otwarte tematy
--------------

* Czy cała komunikacja musi być publiczna? Może powinna część powinna być 
  wysyłana bezpośrednio do bota lub do użytkownia.

Zależności do innych systemów:
------------------------------

* Athena powinna powinna dodatkowo umieć startować bota i przeprowadzać
  doświadczenie. Tj. w chwili kiedy przeprowadzamy doświadczenie w trybie
  automatycznym to athena kontroluje wybrane serie pomiarowe.
* Bot używa wersji protokołu wynegocjowanej przez operatora. Jeśli nie
  może jej obsłużyć komunikuje to operatorowi i odmawia przeprowadzenia
  serii pomiarowej.

Procedura przejmowania głosu na pokoju
**************************************

* Bot wysyła do Atheny wiadomość (TODO link) i prosi o zabranie głosu
  operatorowi.
* Kiedy bot skończy dokonywać serię prosi athenę o oddanie głosu operatorowi.
* Jeśli athena w międzyczasie wyśle ``silf:experiment:stop`` oznacza to że
  wygasła rezerwacja i bot powinien skończyć pomiar.
* Jeśli bot wyjdzie z pokoju do chatu to athena oddaje głos operatorowi.


.. note::

    Naprawdę próbowaliśmy pozbyć się tej wiadomości, ale nie dało się zaprezentować 
    sensownej alernatywy.


Logika przeprowadzania doświadczenia w trybie automatycznym
***********************************************************

W określonych godzinach eksperyment będzie pracował w trybie automatycznym.
Tryb automatyczny jest wykrywany w taki sposób że jest to czas w którym rezerwacje
ma ``silf-bot``.

Taki eksperyment będzie przeprowadzany przez Athenę, na następujących zasadach:


1. Athena informuje bota o włączeniu trybu automatycznego wysyłając mu
   w TODO sposób. Cała reszta logiki jest w bocie.
2. Przeprowadza się po kolei kolejne serie pomiarowe które bot potrafi
   przeprowadzić.
3. Pierwszy cykl serii (wszystkie serie po kolei) przeprowadzany jest zawsze.
4. Kolejne tylko wtedy kiedy na pokoju są jacyś żywi obserwatorzy.
   Przy czym logika byłaby taka:

   * Jeśli ktoś wchodzi do pokoju na którym trwa seria pomiarowa kontynuujemy ją.
   * Jeśli ktoś wchodzi do pokoju w którym nie trwa seria zaczynamy cykl serii
     od początku.
   * Jeśli ostatnia osoba wychodzi z pokoju w którym trwa seria pomiarowa
     kończymy caly cykl serii po np. 5 minutach (w ten sposób kiedy z powodu
     problemów z siecią rozłączy kogoś na chwilę nie będzie musiał obserowować 
     pomiarów rozpoczętych od nowa).


Komunikacja czesio-athena
-------------------------

Są dwie możliwości:

* Jeśli czesio jest częścią atheny na pokoju nie ma żadnej komunikacji, a
  ta odbywa się za pomocą API Atheny.
* Jeśli jednak jest rozdzielony to wprowadzamy cztery wiadomości
  (o niesprecyzowanym, na razie, formacie).


**Format wiadomości**

Pierwsze dwie to wiadomości w których Czesio prosi athenę o nadanie mu uprawnień
do mówienia na pokoju kontrolnym (tą czesio wysyła po otrzymaniu
``\czesio seria start``) a druga prosi o odebranie mu tych uprawnień.

**Wiadomości o rezerwacji**

Drugi zestaw będzie generyczney (wykorzystywane w również do innych funkcjonalności) i
i będzie notyfikować o początku i końcu rezerwacji.

Notyfikacje o początku/trwaniu rezerwacji będzie wysyłana podczas:

1. Czas początku rezerwacji
2. Czas od teraz do końca rezerwacji w GMT
3. Datę końca rezerwacji w GMT

   .. note::

        Mieliśmy w ILF problemy z dziwnymi zegarami u użytkowników, offsety są bezpieczniejsze.

3. Nick/jid osoby posiadającej rezerwację

Wiadomości te będą wysyłane w następujących okolicznościach:

1. Do uzytkownika znajdującego się na pokoju kiedy jego
   rezerwacja się rozpoczyna.

2. Do każdego użytkownika wchodzącego do pokoju, jeśli w danej rezerwacji
 operator pojawił się na pokoju choć raz.

 .. note::

    Jeśli nie było operatora to z definicji nic się nie może na pokoju dziac.

Notyfikacje o końu rezerwacji będą zawierać informację o osobie posiadającej
rezerwacje i będą wywyłane w chwili kończenia rezerwacji.

**Wiadomości o rezerwacji a czesio**

Wiadomości o początku i końcu rezerwacji zawierają nicka osoby posiadającej
rezerwację. Czesio powinien mieć rezerwację w chwili w którym będize
przeprowadzać automatycznie doświadczenie. Otrzymanie informacji o początku
rezerwacji będzie oznaczało że powinien zacząć automatyczne przeprowadzanie
doświadczenia.



Zasoby powiązane z botem
------------------------

Bot powinien logować się na następujące dane:

* Username: ``silf-chatbot``@domain
* Nick: ``silf-chatbot``

Inne funkcjonalności:
---------------------

* Wysyłanie wiadomości e-mail jeśli pojawi się jakiś błąd (wyjątek!), albo
  odstępstwo od oczekiwanych wyników.
* Fajnie byłoby gdyby bot umiał przeprowadzać fitowanie do danych
  tak żeby podał interpretację teoretyczną.