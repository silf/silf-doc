Enviorment setup
================

Tigase server
-------------

Download **recent version** of tigase server (at least one with
websockets).

Configure it to use ``postgres database``.

Web server
----------

Webserver part has following functions:

* Manages experiment reservations
* Creates tigase users from webserver users (hence allows login)

Please download: https://bitbucket.org/silf/silf-app there is a simple
django site that hosts this app, but you'll probably need to
do some additional styling.

Installation procedure is as follows:

* Configure django site to use the same postgres database as tigase
* Update ownership of tigase tables so django databse user has
  proper priveleges (see ``silf-app/doc/`` for documentation)
* Create ``Site`` object in django site.

.. warning::

    Possible gothas: take care if you install ``tigase`` locally. You need
    co carefully and properly devine ``host`` property. By default
    our installer (and tigase installer) use host from ``/etc/hostname/``.

    We had problems when developer had hostname containing upercase characters,
    in this case you should specify ``vhosts`` in ``tigase.ini`` to be
    lowercase, see `this post
    <http://www.tigase.org/content/xep-0114-component-cant-connect-host-unknown>`.

Athena
------

Install athena bot in it's own virtualenv.

Configuration is rather straightforward.

Mercury
-------

Install mercury according to the documentation.

You can configure Mercury in two ways:

* Either use configuration file