Code conventions
================

* We use PEP8 more or less
* In code SILF abbreviation should have only first method capilalized is written as:

  .. code-block:: python

       import silf.whatever

       class SuperSilfClass(object):

            def silf_method(self):
                pass

