.. Silf  documentation master file, created by
   sphinx-quickstart on Sat Dec 14 15:44:55 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Silf 's documentation!
=================================

| Silf is a **remote laboratory**,
| but also a **framework** for your
| remote laboratory.

Components:

.. toctree::
   :maxdepth: 1

   components/commons
   components/athena
   components/silf-app
   components/mercury
   components/doc
   code_conventions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

